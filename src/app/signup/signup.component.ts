import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  constructor(private  httpClient: HttpClient, private router: Router) {
  }

  passsword;
   username;
  email;
  // address;
  // email;
  //
  // state;
  // pincode;
  // phone;
  // city;
  url = 'http://localhost:8080/signup/sendingData';

  ngOnInit() {

  }

  register1() {
    const json = {

      passsword: this.passsword,
      username: this.username,
      // address: this.address,
      email: this.email,
      // city: this.city,
      // state: this.state,
      // phone: this.phone,
      // pincode: this.pincode,
      isactive: 0,

    };

    this.httpClient.post(this.url, json).subscribe(res => {

      this.router.navigate(['/login']);
    });
  }


}

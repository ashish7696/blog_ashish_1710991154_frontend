import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BlogService} from '../blog.service';

@Component({
  selector: 'app-updateblog',
  templateUrl: './updateblog.component.html',
  styleUrls: ['./updateblog.component.scss']
})
export class UpdateblogComponent implements OnInit {
  id1;
  n;
  category;
  details;
  image;


  constructor(private b: BlogService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    const name = this.route.snapshot.paramMap.get('name');
    this.id1 = id;
    this.n = name;

  }

  update() {
    if (confirm('Hey do u want to update')) {
      this.b.update({
        username: this.n,
        blogid: this.id1,
        category: this.category,
        details: this.details,
        image: this.image

      }).subscribe(data => {
        alert('Blog Updatewd!!!!');
      });
      this.router.navigate(['/my']);
    }
  }
}

import {Component, OnInit} from '@angular/core';

import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {UserService} from '../user.service';
import {BlogService} from '../blog.service';

@Component({
  selector: 'app-addblog',
  templateUrl: './addblog.component.html',
  styleUrls: ['./addblog.component.scss']
})
export class AddblogComponent implements OnInit {

  constructor(private us: UserService, private bs: BlogService, private route: Router) {
  }

  public username;
  public category;
  public image;
  public details;
public privacy;
  ngOnInit() {
  }

  add() {
    if (confirm('Are u sure  want to post')) {
      this.bs.addblog({
        username: sessionStorage.getItem('user'),
        category: this.category,
        details: this.details,
        image: this.image,
        privacy: this.privacy
      }).subscribe(data => {
        alert('blog added');
      });
      this.route.navigate(['/user']);
    }
  }
}

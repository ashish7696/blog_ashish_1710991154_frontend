import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {HttpClient} from '@angular/common/http';
import {AuthenticationService} from '../services/authentication.service';
import {AppService} from '../app.service';
import {BlogService} from '../blog.service';
import {UserService} from '../user.service';
import {CommentService} from '../comment.service';
import {FollowService} from '../services/follow.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  logoutUrl = 'http://localhost:8080/login/logout';

  // tslint:disable-next-line:max-line-length
  constructor(private followservice: FollowService, private bs: CommentService, private blogservice: BlogService, private usr: UserService, private router: Router, private httpClient: HttpClient, private loginService: AuthenticationService, private  serv: AppService) {
  }

  public blogs;
  public blogss = [{id: 1}, {id: 2}, {id: 3}];
  public category;
  public u;
  public m;
  public value = 'not following';
  public bool;

  ngOnInit() {
    this.m = sessionStorage.getItem('user');
    this.blogservice.getblogs().subscribe(data => {
      this.blogs = data;
    });
    this.usr.getuser().subscribe(data => {
      this.u = data;
    });
  }

  // checkfollow(id) {
  //
  //   this.followservice.checkfollow(id).subscribe((data) => {
  //     this.bool = data;
  //   });
  //   return this.bool;
  // }


  // print(value: any) {
  //   var con = document.querySelector('#' + value);
  //   console.log(con.value);
  //
  // }


  logout() {
    if (this.serv.checkLogin()) {
      this.loginService.logoutService();

      this.httpClient.get(this.logoutUrl).subscribe(res => {
        alert('Logout Successful');
      });

      this.router.navigate(['/login']);
    }
  }

  checkLogin() {
    return this.serv.checkLogin();
  }

  prof() {
    this.router.navigate(['/profile']);
  }

  addblogg() {
    this.router.navigate(['/addblog']);
  }


  searchbyusername(user) {
    this.blogservice.searchusername(user).subscribe(data => {
      this.blogs = data;
    });
  }

  searchbycategory(category) {

    this.blogservice.searchcategory(category).subscribe(data => {
      this.blogs = data;
    });
  }

  x() {
    this.router.navigate(['/profile']);
  }

  //
  // userpath() {
  //   this.router.navigate(['/user']);
  // }

  my() {
    this.router.navigate(['/my']);
  }

  follow(id) {

    this.followservice.Follow(id).subscribe(data => {
      alert('user followed');
    });
  }

  unfollow(id) {
    this.value = 'Following';
    this.followservice.Unfollow(id).subscribe(data => {
      alert('user Unfollowed');
    });
  }
}


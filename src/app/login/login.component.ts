import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AppService} from '../app.service';
import {HttpClient} from '@angular/common/http';
import {AuthenticationService} from '../services/authentication.service';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username;
  password;
  email;
  phone;
  showPassword = 'password';
  url = 'http://localhost:8080/signup/sendingData';

  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private loginservice: AuthenticationService, private service: AppService, private httpClient: HttpClient, private formBuilder: FormBuilder ) {
  }

  // constructor() {}
  ngOnInit() {

    if (this.service.checkLogin()) {
      this.router.navigate(['/navbar']);
    }
  }

  sign() {
    this.router.navigate(['/signup']);
  }

  login() {

    this.loginservice.authenticate(this.username, this.password).subscribe(
      data => {
        this.service.isLoggedIn(true);
        this.router.navigate(['/navbar']);
        sessionStorage.setItem('user', this.username);
        sessionStorage.setItem('password', this.password);
        // sessionStorage.setItem('email', this.email);
        // sessionStorage.setItem('phone', this.phone);
      }
    );
  }

  f() {
    this.router.navigate(['/home']);
  }
}

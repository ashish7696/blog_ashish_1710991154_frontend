import { Component, OnInit } from '@angular/core';
import {BlogService} from '../blog.service';
import {Router} from '@angular/router';
import {UserService} from '../user.service';

@Component({
  selector: 'app-myblog',
  templateUrl: './myblog.component.html',
  styleUrls: ['./myblog.component.scss']
})
export class MyblogComponent implements OnInit {

  constructor(private bs: BlogService, private  us: UserService, private  route: Router) {
  }

  public blogs;
  public user;
  public username;
  public id;

  ngOnInit() {
    this.username = sessionStorage.getItem('user');
    console.log(this.username);
    this.bs.searchusername(this.username).subscribe(data => {
      this.blogs = data;

    });
  }

  // blogcomponent() {
  //   this.route.navigate(['/blog']);
  // }

  // delete() {
  //   this.user = sessionStorage.getItem('user');
  //   this.us.deleteuser(this.user).subscribe(data => {
  //     alert('user deleted');
  //   });
  //   this.route.navigate(['/login']);
  // }

  deletebyid(id) {
    this.bs.deleteblogbyid(id).subscribe(data => {
      alert('blog deleted');
      window.location.reload();
            });
  }

  update(id, name) {
    this.route.navigate(['/ublog/' + id + '/' + name]);

  }

  // updateuser() {
  //   //   this.route.navigate(['/updateuser/' + this.username]);
  //   // }
}

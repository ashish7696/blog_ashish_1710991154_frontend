import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProfileserviceService {

  constructor(private httpClient: HttpClient) {
  }


  getuser() {
    const headers = new HttpHeaders({Authorization: 'Basic ' + sessionStorage.getItem('token')});
    return this.httpClient.get('http://localhost:8080/api/user', {headers});
  }
}


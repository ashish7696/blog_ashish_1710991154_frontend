import {Component, OnInit} from '@angular/core';
import {ProfileserviceService} from './profileservice.service';
import {BlogService} from '../blog.service';
import {UserService} from '../user.service';
import {Router} from '@angular/router';
import {FollowService} from '../services/follow.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(private ss: ProfileserviceService, private  us: UserService, private route: Router, private followservice: FollowService) {
  }

  kk;

  date = new Date();

  h;
  pass;
  public user;
  public passsword;
  email;
  phone;
  foll;
  followers;

  ngOnInit() {

    this.h = sessionStorage.getItem('user');
    this.pass = sessionStorage.getItem('password');
    this.email = sessionStorage.getItem('email');
    this.phone = sessionStorage.getItem('phone');
    this.ss.getuser().subscribe((data) => {
      this.kk = data;
    });
    this.followservice.getFollowing().subscribe((data1) => {
      this.foll = data1;
    });
    this.followservice.getFollowers().subscribe((data2) => {
      this.followers = data2;
    });


  }

  // update() {
  //   this.us.updateuser({
  //     username: this.user,
  //     passsword: this.passsword,
  //     // email: this.email,
  //     // phone: this.phone
  //
  //   }).subscribe(data => {
  //     alert('profile updated');
  //   });
  //   this.route.navigate(['/login']);
  // }
}




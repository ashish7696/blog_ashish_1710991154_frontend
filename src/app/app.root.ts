import {Routes} from '@angular/router';
import {NavbarComponent} from './navbar/navbar.component';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {AddblogComponent} from './addblog/addblog.component';
import {MyblogComponent} from './myblog/myblog.component';
import {UpdateblogComponent} from './updateblog/updateblog.component';
import {ProfileComponent} from './profile/profile.component';

export const MAINS: Routes = [
    {path: '', redirectTo: '/login', pathMatch: 'full'},
    {path: 'navbar', component: NavbarComponent},
    {path: 'login', component: LoginComponent},
    {path: 'signup', component: SignupComponent},
    {path: 'addblog', component: AddblogComponent},
    {path: 'my', component: MyblogComponent},
    {path: 'ublog/:id/:name', component: UpdateblogComponent},
    {path: 'profile', component: ProfileComponent}
  ]
;

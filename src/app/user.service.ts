import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

export interface User {
  user_id: number;
  name: string;
  phoneNumber: string;
  password: string;
  status: boolean;
  userName: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private  http: HttpClient) {
  }

  // updateuser(user) {
  //   const token = sessionStorage.getItem('token');
  //   const headers = new HttpHeaders({Authorization: 'Basic ' + token});
  //   return this.http.put<User>('http://localhost:8080/signup/updateUserProfile', user, {headers});
  // }

  getuser() {
    return this.http.get('http://localhost:8080/api/user-get');
  }

}

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Blogsinterface} from './Blogsinterface';


@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient) {
  }

  getblogs(): Observable<Blogsinterface> {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.http.get<Blogsinterface>('http://localhost:8080/blogs/getblogs', {headers});
  }

  searchusername(username) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.http.get<Blogsinterface>('http://localhost:8080/blogs/getblogs/' + username, {headers});
  }

  searchcategory(category) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.http.get<Blogsinterface>('http://localhost:8080/blogs/getblog/' + category, {headers});
  }

  addblog(blog) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.http.post<Blogsinterface>('http://localhost:8080/blogs/addblog/', blog, {headers});
  }

  deleteblogbyid(id) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.http.delete('http://localhost:8080/blogs/deleteblog/' + id, {headers});
  }

  update(blog) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.http.put<Blogsinterface>('http://localhost:8080/blogs/updateblog/', blog, {headers});
  }


  // addComment() {
  //
  //   return this.http.post('http://localhost:8080/comment/add');
  // }
}

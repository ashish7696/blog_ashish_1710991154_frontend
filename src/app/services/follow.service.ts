import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FollowService {

  constructor(private  http: HttpClient) {
  }

  Follow(id) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.http.post('http://localhost:8080/follower/follow/' + id, null, {headers});
  }

  Unfollow(id) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.http.post('http://localhost:8080/follower/unfollow/' + id, null, {headers});
  }

  getFollowing() {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.http.get('http://localhost:8080/follower/following', {headers});
  }

  getFollowers() {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.http.get('http://localhost:8080/follower/get-followers', {headers});
  }

  // checkfollow(id) {
  //   const token = sessionStorage.getItem('token');
  //   const headers = new HttpHeaders({Authorization: 'Basic ' + token});
  //   return this.http.get('http://localhost:8080/follower/followed' + '?id=' + id, {headers});
  // }
}

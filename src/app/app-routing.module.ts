import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MAINS} from './app.root';


const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(MAINS)],
  exports: [RouterModule]

})
export class AppRoutingModule { }
